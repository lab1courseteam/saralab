package BLL;

import BLL.Grupi;
import BLL.Mesuesja;
import BLL.Planifikimi;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Enrollment.class)
public class Enrollment_ { 

    public static volatile SingularAttribute<Enrollment, Integer> eid;
    public static volatile SingularAttribute<Enrollment, Grupi> grupiID;
    public static volatile SingularAttribute<Enrollment, Mesuesja> mid;
    public static volatile SingularAttribute<Enrollment, Planifikimi> planifikimiID;

}