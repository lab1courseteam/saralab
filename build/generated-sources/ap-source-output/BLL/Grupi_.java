package BLL;

import BLL.Enrollment;
import BLL.GrupMosha;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Grupi.class)
public class Grupi_ { 

    public static volatile SingularAttribute<Grupi, Integer> grupiID;
    public static volatile SingularAttribute<Grupi, String> grupi;
    public static volatile CollectionAttribute<Grupi, GrupMosha> grupMoshaCollection;
    public static volatile CollectionAttribute<Grupi, Enrollment> enrollmentCollection;

}