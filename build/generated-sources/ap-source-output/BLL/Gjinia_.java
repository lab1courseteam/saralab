package BLL;

import BLL.Femiu;
import BLL.Mesuesja;
import BLL.Prindi;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Gjinia.class)
public class Gjinia_ { 

    public static volatile SingularAttribute<Gjinia, Integer> gjiniaID;
    public static volatile CollectionAttribute<Gjinia, Mesuesja> mesuesjaCollection;
    public static volatile CollectionAttribute<Gjinia, Femiu> femiuCollection;
    public static volatile SingularAttribute<Gjinia, String> gjinia;
    public static volatile CollectionAttribute<Gjinia, Prindi> prindiCollection;

}