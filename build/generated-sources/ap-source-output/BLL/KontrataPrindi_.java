package BLL;

import BLL.Kontrata;
import BLL.Prindi;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(KontrataPrindi.class)
public class KontrataPrindi_ { 

    public static volatile SingularAttribute<KontrataPrindi, Integer> kpid;
    public static volatile SingularAttribute<KontrataPrindi, Kontrata> kid;
    public static volatile SingularAttribute<KontrataPrindi, Prindi> pid;

}