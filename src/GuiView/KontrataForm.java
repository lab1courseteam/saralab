
package GuiView;

import BLL.Kontrata;
import DAL.CrudFormException;
import DAL.KontrataInterface;
import DAL.KontrataRepository;
import GuiModel.KontrataTableModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class KontrataForm extends javax.swing.JInternalFrame {

     KontrataInterface ki = new KontrataRepository();
    KontrataTableModel ktm = new KontrataTableModel();
    public KontrataForm() {
        initComponents();
         loadTable();
        tabelaSelectedIndexChange();
    }
        public void loadTable() {
        try {
            List<Kontrata> lista = ki.findAll();
            ktm.add(lista);
            tabela.setModel(ktm);
            ktm.fireTableDataChanged();
        } catch (CrudFormException ene) {
            JOptionPane.showMessageDialog(this, ene.getMessage());
        }

    }

    private void tabelaSelectedIndexChange() {
        final ListSelectionModel rowSM = tabela.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    Kontrata en = ktm.getKontrata(selectedIndex);

                    txtID.setText(en.getKid() + "");
                    Dataf.setDate(en.getDataFillimit());
                    DataM.setDate(en.getDataMbarimit());

                }
            }
        });
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        txtDataF = new javax.swing.JTextField();
        txtDataM = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabela = new javax.swing.JTable();
        savebtn = new javax.swing.JButton();
        editbtn = new javax.swing.JButton();
        clearbtn = new javax.swing.JButton();
        deletebtn = new javax.swing.JButton();
        Dataf = new com.toedter.calendar.JDateChooser();
        DataM = new com.toedter.calendar.JDateChooser();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        txtDataF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDataFActionPerformed(evt);
            }
        });

        setClosable(true);
        setMaximizable(true);
        setResizable(true);

        jLabel1.setText("Kontrata ID:");

        jLabel2.setText("Data e fillimit :");

        jLabel3.setText("Data e mbarimit :");

        jLabel4.setText("Femiu :");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        tabela.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tabela);

        savebtn.setText("Save");
        savebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savebtnActionPerformed(evt);
            }
        });

        editbtn.setText("Edit");
        editbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editbtnActionPerformed(evt);
            }
        });

        clearbtn.setText("Clear");
        clearbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearbtnActionPerformed(evt);
            }
        });

        deletebtn.setText("Delete");
        deletebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletebtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Dataf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DataM, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(59, 59, 59)
                .addComponent(savebtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(editbtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(clearbtn)
                .addGap(18, 18, 18)
                .addComponent(deletebtn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 680, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(Dataf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(savebtn)
                        .addComponent(editbtn)
                        .addComponent(clearbtn)
                        .addComponent(deletebtn))
                    .addComponent(DataM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtDataFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDataFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDataFActionPerformed

    private void savebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savebtnActionPerformed
        // TODO add your handling code here:
             int row = tabela.getSelectedRow();
        if (!txtDataF.getText().equals("")) {
            if (row == -1) {
                Kontrata en = new Kontrata();
                en.setKid(Integer.parseInt(txtID.getText()));
                en.setDataFillimit(Dataf.getDate());
                en.setDataMbarimit(DataM.getDate());
                try {
                    ki.create(en);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
                Kontrata en = ktm.getKontrata(row);
                en.setDataFillimit(Dataf.getDate());
                en.setDataMbarimit(DataM.getDate());

                try {
                    ki.edit(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(KontrataForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            clearField();
            loadTable();
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }//GEN-LAST:event_savebtnActionPerformed

    private void editbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editbtnActionPerformed
        // TODO add your handling code here:
            int row = tabela.getSelectedRow();
        if (!txtDataF.getText().equals("")) {
            if (row == -1) {
                Kontrata en = new Kontrata();
                en.setKid(Integer.parseInt(txtID.getText()));
                en.setDataFillimit(Dataf.getDate());
                en.setDataMbarimit(DataM.getDate());
                try {
                    ki.create(en);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
                Kontrata en = ktm.getKontrata(row);
                en.setDataFillimit(Dataf.getDate());
                en.setDataMbarimit(DataM.getDate());

                try {
                    ki.edit(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(KontrataForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            clearField();
            loadTable();
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }//GEN-LAST:event_editbtnActionPerformed

    private void clearbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearbtnActionPerformed
        // TODO add your handling code here:
         clearField();
    }//GEN-LAST:event_clearbtnActionPerformed

    private void deletebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletebtnActionPerformed
        // TODO add your handling code here:
         int row = tabela.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i
                    = JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja",
                            JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                Kontrata en = ktm.getKontrata(row);
                try {
                    ki.delete(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(KontrataForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                clearField();
                loadTable();
            } else {
                clearField();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }
    }//GEN-LAST:event_deletebtnActionPerformed


     private void clearField() {
        tabela.clearSelection();
        txtID.setText(" ");
        Dataf.setDate(null);
        DataM.setDate(null);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser DataM;
    private com.toedter.calendar.JDateChooser Dataf;
    private javax.swing.JButton clearbtn;
    private javax.swing.JButton deletebtn;
    private javax.swing.JButton editbtn;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton savebtn;
    private javax.swing.JTable tabela;
    private javax.swing.JTextField txtDataF;
    private javax.swing.JTextField txtDataM;
    private javax.swing.JTextField txtID;
    // End of variables declaration//GEN-END:variables
}
