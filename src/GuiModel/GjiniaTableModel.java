
package GuiModel;

import BLL.Gjinia;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class GjiniaTableModel extends AbstractTableModel{
      private final String [] columnNames = {"GjiniaID","Gjinia"};
    private List <Gjinia> data;
    public GjiniaTableModel(List<Gjinia>data){
        this.data = data;
    }
    public GjiniaTableModel() {
    }
    public void add(List<Gjinia>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Gjinia getGjinia(int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Gjinia en = (Gjinia)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getGjiniaID();
            case 1:
                return en.getGjinia();
            
            default:
                return null;
        }
    }
}
