package BLL;

import BLL.Enrollment;
import BLL.Gjinia;
import BLL.MesuejaKontrate;
import BLL.Niveli;
import BLL.Qyteti;
import BLL.Shteti;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Mesuesja.class)
public class Mesuesja_ { 

    public static volatile SingularAttribute<Mesuesja, String> emri;
    public static volatile SingularAttribute<Mesuesja, Shteti> shteti;
    public static volatile CollectionAttribute<Mesuesja, Enrollment> enrollmentCollection;
    public static volatile SingularAttribute<Mesuesja, Integer> mid;
    public static volatile SingularAttribute<Mesuesja, String> mbiemri;
    public static volatile SingularAttribute<Mesuesja, Gjinia> gjinia;
    public static volatile CollectionAttribute<Mesuesja, Niveli> niveliCollection;
    public static volatile SingularAttribute<Mesuesja, Qyteti> qyteti;
    public static volatile CollectionAttribute<Mesuesja, MesuejaKontrate> mesuejaKontrateCollection;

}