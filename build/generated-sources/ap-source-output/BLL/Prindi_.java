package BLL;

import BLL.Gjinia;
import BLL.KontrataPrindi;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Prindi.class)
public class Prindi_ { 

    public static volatile SingularAttribute<Prindi, String> emri;
    public static volatile SingularAttribute<Prindi, Integer> nrTel;
    public static volatile SingularAttribute<Prindi, Integer> pid;
    public static volatile SingularAttribute<Prindi, String> mbiemri;
    public static volatile SingularAttribute<Prindi, Gjinia> gjinia;
    public static volatile SingularAttribute<Prindi, String> email;
    public static volatile CollectionAttribute<Prindi, KontrataPrindi> kontrataPrindiCollection;

}