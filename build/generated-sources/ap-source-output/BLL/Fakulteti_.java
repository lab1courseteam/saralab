package BLL;

import BLL.Drejtimi;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Fakulteti.class)
public class Fakulteti_ { 

    public static volatile SingularAttribute<Fakulteti, Integer> ftID;
    public static volatile SingularAttribute<Fakulteti, String> fakulteti;
    public static volatile CollectionAttribute<Fakulteti, Drejtimi> drejtimiCollection;

}