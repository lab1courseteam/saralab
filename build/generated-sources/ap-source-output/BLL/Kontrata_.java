package BLL;

import BLL.Femiu;
import BLL.GrupMosha;
import BLL.KontrataPrindi;
import BLL.MesuejaKontrate;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Kontrata.class)
public class Kontrata_ { 

    public static volatile SingularAttribute<Kontrata, Femiu> fid;
    public static volatile SingularAttribute<Kontrata, Date> dataMbarimit;
    public static volatile CollectionAttribute<Kontrata, GrupMosha> grupMoshaCollection;
    public static volatile SingularAttribute<Kontrata, Integer> kid;
    public static volatile SingularAttribute<Kontrata, Date> dataFillimit;
    public static volatile CollectionAttribute<Kontrata, MesuejaKontrate> mesuejaKontrateCollection;
    public static volatile CollectionAttribute<Kontrata, KontrataPrindi> kontrataPrindiCollection;

}