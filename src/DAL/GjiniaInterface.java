
package DAL;

import BLL.Gjinia;
import java.util.List;


public interface GjiniaInterface {
       void create(Gjinia p) throws CrudFormException;
    void edit(Gjinia p) throws CrudFormException;
    void delete(Gjinia p) throws CrudFormException;
    List<Gjinia> findAll() throws CrudFormException;
    Gjinia findByID(Integer ID) throws CrudFormException;
}
