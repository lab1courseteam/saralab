package BLL;

import BLL.Enrollment;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Planifikimi.class)
public class Planifikimi_ { 

    public static volatile CollectionAttribute<Planifikimi, Enrollment> enrollmentCollection;
    public static volatile SingularAttribute<Planifikimi, Integer> planifikimiID;

}