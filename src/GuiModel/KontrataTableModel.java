
package GuiModel;

import BLL.Kontrata;
import java.util.List;
import javax.swing.table.AbstractTableModel;


public class KontrataTableModel extends AbstractTableModel {
     private final String [] columnNames = {"ID","dataFillimit","dataMbarimit"};
    private List <Kontrata> data;
    public KontrataTableModel(List<Kontrata>data){
        this.data = data;
    }
    public KontrataTableModel() {
    }
    public void add(List<Kontrata>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Kontrata getKontrata(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Kontrata en = (Kontrata)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getKid();
            case 1:
                return en.getDataFillimit();
            case 2:
                return en.getDataMbarimit();
            default:
                return null;
        }
    }
}
