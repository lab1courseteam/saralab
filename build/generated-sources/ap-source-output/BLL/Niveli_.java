package BLL;

import BLL.Drejtimi;
import BLL.Mesuesja;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Niveli.class)
public class Niveli_ { 

    public static volatile SingularAttribute<Niveli, String> niveli;
    public static volatile SingularAttribute<Niveli, Drejtimi> drejtimiID;
    public static volatile SingularAttribute<Niveli, Mesuesja> mesuesjaID;
    public static volatile SingularAttribute<Niveli, Integer> niveliID;

}