
package DAL;

import BLL.Kontrata;
import java.util.List;


public interface KontrataInterface {
     void create(Kontrata p) throws CrudFormException;
    void edit(Kontrata p) throws CrudFormException;
    void delete(Kontrata p) throws CrudFormException;
    List<Kontrata> findAll() throws CrudFormException;
    Kontrata findByID(Integer ID) throws CrudFormException;
  
}
