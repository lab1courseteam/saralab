                          
package DAL;

import BLL.Kontrata;
import java.util.List;
import javax.persistence.Query;


public class KontrataRepository extends EntMngClass implements KontrataInterface{
       @Override
    public void create(Kontrata en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(Kontrata en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(Kontrata en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<Kontrata> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Kontrata.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Kontrata findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Kontrata e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Kontrata)query.getSingleResult();
    }
  
  
}
