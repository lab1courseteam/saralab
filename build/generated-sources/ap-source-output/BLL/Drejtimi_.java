package BLL;

import BLL.Fakulteti;
import BLL.Niveli;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Drejtimi.class)
public class Drejtimi_ { 

    public static volatile SingularAttribute<Drejtimi, String> drejtimi;
    public static volatile SingularAttribute<Drejtimi, Fakulteti> fakultetiID;
    public static volatile CollectionAttribute<Drejtimi, Niveli> niveliCollection;
    public static volatile SingularAttribute<Drejtimi, Integer> drejtimiID;

}