package BLL;

import BLL.Femiu;
import BLL.Mesuesja;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Qyteti.class)
public class Qyteti_ { 

    public static volatile SingularAttribute<Qyteti, String> emri;
    public static volatile SingularAttribute<Qyteti, String> kodiPostar;
    public static volatile CollectionAttribute<Qyteti, Mesuesja> mesuesjaCollection;
    public static volatile CollectionAttribute<Qyteti, Femiu> femiuCollection;
    public static volatile SingularAttribute<Qyteti, Integer> qid;

}