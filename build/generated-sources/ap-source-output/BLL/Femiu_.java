package BLL;

import BLL.Gjinia;
import BLL.Kontrata;
import BLL.Qyteti;
import BLL.Shendeti;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-10T13:40:36")
@StaticMetamodel(Femiu.class)
public class Femiu_ { 

    public static volatile SingularAttribute<Femiu, Integer> fid;
    public static volatile SingularAttribute<Femiu, String> emri;
    public static volatile SingularAttribute<Femiu, Shendeti> shendeti;
    public static volatile SingularAttribute<Femiu, String> emriPrindit;
    public static volatile CollectionAttribute<Femiu, Kontrata> kontrataCollection;
    public static volatile SingularAttribute<Femiu, Integer> shteti;
    public static volatile SingularAttribute<Femiu, String> adresa;
    public static volatile SingularAttribute<Femiu, String> mbiemri;
    public static volatile SingularAttribute<Femiu, Date> dataLindjes;
    public static volatile SingularAttribute<Femiu, Gjinia> gjinia;
    public static volatile SingularAttribute<Femiu, Qyteti> qyteti;

}